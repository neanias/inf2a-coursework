
// File:   MH_Lexer.java
// Date:   October 2015

// Java template file for lexer component of Informatics 2A Assignment 1 (2015).
// Concerns lexical classes and lexer for the language MH (`Micro-Haskell').


import java.io.* ;

class MH_Lexer extends GenLexer implements LEX_TOKEN_STREAM {

static class VarAcceptor extends GenAcceptor implements DFA {

  public String lexClass() { return "VAR"; }
  public int numberOfStates() { return 3; }

  int nextState (int state, char c) {
    switch (state) {
      case 0:
        return CharTypes.isSmall(c) ? 1 : 2;
      case 1:
        if (CharTypes.isLetter(c) || CharTypes.isDigit(c) || c == '\'')
          return 1;
        else
          return 2;
      default:
        return 2;
    }
  }

  boolean accepting (int state) { return (state == 1); }
  boolean dead (int state) { return (state == 2); }
}

static class NumAcceptor extends GenAcceptor implements DFA {

  public String lexClass() { return "NUM"; }
  public int numberOfStates() { return 3; }

  int nextState (int state, char c) {
    switch (state) {
      case 0:
        return CharTypes.isDigit(c) ? 1 : 2;
      case 1:
        return CharTypes.isDigit(c) ? 1 : 2;
      default:
        return 2;
    }
  }

  boolean accepting (int state) { return (state == 1); }
  boolean dead (int state) { return (state == 2); }
}

static class BooleanAcceptor extends GenAcceptor implements DFA {

  public String lexClass() { return "BOOLEAN"; }
  public int numberOfStates() { return 11; }

  int nextState (int state, char c) {
    switch (state) {
      case 0:
        if (c == 'T') return 1;
        else if (c == 'F') return 5;
        else return 10;
      case 1:
        return (c == 'r') ? 2 : 10;
      case 2:
        return (c == 'u') ? 3 : 10;
      case 3:
        return (c == 'e') ? 4 : 10;
      case 4:
        return 10;
      case 5:
        return (c == 'a') ? 6 : 10;
      case 6:
        return (c == 'l') ? 7 : 10;
      case 7:
        return (c == 's') ? 8 : 10;
      case 8:
        return (c == 'e') ? 9 : 10;
      case 9:
        return 10;
      default:
        return 10;
    }
  }

  boolean accepting (int state) { return (state == 4 || state == 9); }
  boolean dead (int state) { return (state == 10); }
}

static class SymAcceptor extends GenAcceptor implements DFA {

  public String lexClass() { return "SYM"; }
  public int numberOfStates() { return 3; }

  int nextState (int state, char c) {
    switch (state) {
      case 0:
        return CharTypes.isSymbolic(c) ? 1 : 2;
      case 1:
        return CharTypes.isSymbolic(c) ? 1 : 2;
      default:
        return 2;
    }
  }

  boolean accepting (int state) { return (state == 1); }
  boolean dead (int state) { return (state == 2); }
}

static class WhitespaceAcceptor extends GenAcceptor implements DFA {

  public String lexClass() { return ""; }
  public int numberOfStates() { return 3; }

  int nextState (int state, char c) {
    switch (state) {
      case 0:
        return CharTypes.isWhitespace(c) ? 1 : 2;
      default:
        return 2;
    }
  }

  boolean accepting (int state) { return (state == 1); }
  boolean dead (int state) { return (state == 2); }
}

static class CommentAcceptor extends GenAcceptor implements DFA {

  public String lexClass() { return ""; }
  public int numberOfStates() { return 5; } // TODO

  int nextState (int state, char c) {
    switch (state) {
      case 0:
        return (c == '-') ? 1 : 4;
      case 1:
        return (c == '-') ? 2 : 4;
      case 2:
        if (c == '-')
          return 3;
        else if (!(CharTypes.isSymbolic(c) || CharTypes.isNewline(c)))
          return 3;
        else
          return 4;
      case 3:
        return (!CharTypes.isNewline(c)) ? 3 : 4;
      default:
        return 4;
    }
  }

  boolean accepting (int state) { return (state == 2 || state == 3); }
  boolean dead (int state) { return (state == 4); }
}

static class TokAcceptor extends GenAcceptor implements DFA {

  String tok;
  int tokLen;
  TokAcceptor (String tok) { this.tok = tok ; tokLen = tok.length(); }

  public String lexClass() { return tok; }
  public int numberOfStates() { return (tokLen + 2); }

  int nextState (int state, char c) {
    if (state >= tokLen)
      return tokLen + 1;
    else if (c == tok.charAt(state))
      return (state + 1);
    else
      return tokLen + 1;
  }

  boolean accepting (int state) { return (state == tokLen); }
  boolean dead (int state) { return (state == tokLen + 1); }
}

    static DFA varAcc        = new VarAcceptor();
    static DFA numAcc        = new NumAcceptor();
    static DFA booleanAcc    = new BooleanAcceptor();
    static DFA symAcc        = new SymAcceptor();
    static DFA whitespaceAcc = new WhitespaceAcceptor();
    static DFA commentAcc    = new CommentAcceptor();
    static DFA integerAcc    = new TokAcceptor("Integer");
    static DFA boolAcc       = new TokAcceptor("Bool");
    static DFA ifAcc         = new TokAcceptor("if");
    static DFA thenAcc       = new TokAcceptor("then");
    static DFA elseAcc       = new TokAcceptor("else");
    static DFA lbracAcc      = new TokAcceptor("(");
    static DFA rbracAcc      = new TokAcceptor(")");
    static DFA semicolonAcc  = new TokAcceptor(";");

    static DFA[] MH_acceptors = new DFA[] {
      whitespaceAcc,
      lbracAcc,
      ifAcc,
      thenAcc,
      elseAcc,
      commentAcc,
      integerAcc,
      booleanAcc,
      varAcc,
      symAcc,
      numAcc,
      boolAcc,
      rbracAcc,
      semicolonAcc
    };

    MH_Lexer (Reader reader) {
      super(reader, MH_acceptors) ;
    }

}
