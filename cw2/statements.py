# File: statements.py
# Template file for Informatics 2A Assignment 2:
# 'A Natural Language Query System in Python/NLTK'

# John Longley, November 2012
# Revised November 2013 and November 2014 with help from Nikolay Bogoychev
# Revised November 2015 by Toms Bergmanis and Shay Cohen


# PART A: Processing statements

def add(lst, item):
    if (item not in lst):
        lst.insert(len(lst), item)


class Lexicon:

    """stores known word stems of various part-of-speech categories"""

    def __init__(self):
        self.lexicon = {}

    def add(self, stem, cat):
        if cat in self.lexicon:
            self.lexicon[cat].add(stem)
        else:
            self.lexicon[cat] = {stem}

    def getAll(self, cat):
        if cat in self.lexicon:
            return list(self.lexicon[cat])
        else:
            return []


class FactBase:

    def __init__(self):
        self.factbase = {}

    def addUnary(self, pred, e1):
        if pred in self.factbase:
            self.factbase[pred].add(e1)
        else:
            self.factbase[pred] = {e1}

    def addBinary(self, pred, e1, e2):
        if pred in self.factbase:
            self.factbase[pred].add((e1, e2))
        else:
            self.factbase[pred] = {(e1, e2)}

    def queryUnary(self, pred, e1):
        return pred in self.factbase and e1 in self.factbase[pred]

    def queryBinary(self, pred, e1, e2):
        return pred in self.factbase and (e1, e2) in self.factbase[pred]


import re
from nltk.corpus import brown

# For super fast brown lookup! (After initial conversion, anyway)
brown_tagged_words = set(brown.tagged_words())


def verb_stem(s):
    """extracts the stem from the 3sg form of a verb, or returns empty string"""
    if (s, 'VBZ') in brown_tagged_words:
        return to_first_person(s)
    else:
        return ''


def to_first_person(s):
    if re.match(r'^(\w|unt)(ies)', s):
        return s[:-1]
    elif re.match(r'\w+[aeiou]{1}ys', s):
        return s[:-1]
    elif re.match(r'\w{2,}[^aeiou]?ies', s):
        return (s[:-3]) + 'y'
    elif re.match(r'\w+[^zs]{1}(s|z)e', s):
        return s[:-1]
    elif s == 'has':
        return 'have'
    elif re.match(r'^\w+(o|x|ch|sh|ss|zz)es', s):
        return s[:-2]
    elif re.match(r'\w+[^aeiousxyz]{1}(?!(ch|sh))s', s):
        return s[:-1]
    elif re.match(r'^\w+[^iosxz]?(?!(ch|sh)){1}es', s):
        return s[:-1]
    else:
        return ''


def add_proper_name(w, lx):
    """adds a name to a lexicon, checking if first letter is uppercase"""
    if ('A' <= w[0] and w[0] <= 'Z'):
        lx.add(w, 'P')
        return ''
    else:
        return (w + " isn't a proper name")


def process_statement(lx, wlist, fb):
    """analyses a statement and updates lexicon and fact base accordingly;
       returns '' if successful, or error message if not."""
    # Grammar for the statement language is:
    #   S  -> P is AR Ns | P is A | P Is | P Ts P
    #   AR -> a | an
    # We parse this in an ad hoc way.
    msg = add_proper_name(wlist[0], lx)
    if (msg == ''):
        if (wlist[1] == 'is'):
            if (wlist[2] in ['a', 'an']):
                lx.add(wlist[3], 'N')
                fb.addUnary('N_' + wlist[3], wlist[0])
            else:
                lx.add(wlist[2], 'A')
                fb.addUnary('A_' + wlist[2], wlist[0])
        else:
            stem = verb_stem(wlist[1])
            if (len(wlist) == 2):
                lx.add(stem, 'I')
                fb.addUnary('I_' + stem, wlist[0])
            else:
                msg = add_proper_name(wlist[2], lx)
                if (msg == ''):
                    lx.add(stem, 'T')
                    fb.addBinary('T_' + stem, wlist[0], wlist[2])
    return msg

# End of PART A.
