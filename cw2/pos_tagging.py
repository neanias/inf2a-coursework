# File: pos_tagging.py
# Template file for Informatics 2A Assignment 2:
# 'A Natural Language Query System in Python/NLTK'

# John Longley, November 2012
# Revised November 2013 and November 2014 with help from Nikolay Bogoychev
# Revised November 2015 by Toms Bergmanis and Shay Cohen


# PART B: POS tagging

from statements import *

# The tagset we shall use is:
# P  A  Ns  Np  Is  Ip  Ts  Tp  BEs  BEp  DOs  DOp  AR  AND  WHO  WHICH  ?

# Tags for words playing a special role in the grammar:

function_words_tags = [('a', 'AR'), ('an', 'AR'), ('and', 'AND'),
                       ('is', 'BEs'), ('are', 'BEp'), ('does', 'DOs'),
                       ('do', 'DOp'), ('who', 'WHO'), ('which', 'WHICH'),
                       ('Who', 'WHO'), ('Which', 'WHICH'), ('?', '?')]
# upper or lowercase tolerated at start of question.

function_words = [p[0] for p in function_words_tags]


def unchanging_plurals():
    singulars = set()
    plurals = set()
    with open("sentences.txt", "r") as f:
        for line in f:
            tags = line.split()
            words = [tag.split('|') for tag in tags]
            for word in words:
                if word[1] == 'NN':
                    singulars.add(word[0])
                elif word[1] == 'NNS':
                    plurals.add(word[0])

    return list(singulars & plurals)


unchanging_plurals_list = unchanging_plurals()


def to_first_person(s):
    if re.match(r'^(\w|unt)(ies)', s):
        return s[:-1]
    elif re.match(r'\w+[aeiou]{1}ys', s):
        return s[:-1]
    elif re.match(r'\w{2,}[^aeiou]?ies', s):
        return (s[:-3]) + 'y'
    elif re.match(r'\w+[^zs]{1}(s|z)e', s):
        return s[:-1]
    elif s == 'has':
        return 'have'
    elif re.match(r'^\w+(o|x|ch|sh|ss|zz)es', s):
        return s[:-2]
    elif re.match(r'\w+[^aeiousxyz]{1}(?!(ch|sh))s', s):
        return s[:-1]
    elif re.match(r'^\w+[^iosxz]?(?!(ch|sh)){1}es', s):
        return s[:-1]
    else:
        return ''


def noun_stem(s):
    """extracts the stem from a plural noun, or returns empty string"""
    if s in unchanging_plurals_list:
        return s
    elif s.endswith('men'):
        return s[:-3] + 'man'
    else:
        return to_first_person(s)


def tag_word(lx, wd):
    """returns a list of all possible tags for wd relative to lx"""
    tagged_words = set()
    possibles = [wd, noun_stem(wd), verb_stem(wd)]
    tags = {'P', 'A', 'I', 'N', 'T'}  # It spells a word!

    for tag in tags:
        words = lx.getAll(tag)
        in_words = (possibles[0] in words or possibles[1] in words
                    or possibles[2] in words)
        if in_words:
            if tag == 'N':
                if possibles[0] == possibles[1]:
                    tagged_words.add('Ns')
                    tagged_words.add('Np')
                elif possibles[1]:
                    tagged_words.add('Np')
                else:
                    tagged_words.add('Ns')
            elif tag == 'T' or tag == 'I':
                if possibles[2]:
                    tagged_words.add(tag + 's')
                else:
                    tagged_words.add(tag + 'p')
            else:
                tagged_words.add(tag)

    for tag in function_words_tags:
        if tag[0] == possibles[0]:
            tagged_words.add(tag[1])

    return list(tagged_words)


def tag_words(lx, wds):
    """returns a list of all possible taggings for a list of words"""
    if (wds == []):
        return [[]]
    else:
        tag_first = tag_word(lx, wds[0])
        tag_rest = tag_words(lx, wds[1:])
        return [[fst] + rst for fst in tag_first for rst in tag_rest]

# End of PART B.
